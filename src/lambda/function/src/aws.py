#!/usr/bin/env python3
"""Execute Kubernetes manifests on an AWS EKS cluster

This module can be used as a standalone AWS Lambda function, or AWS 
Lambda-backed CloudFormation custom resource.

This module requires an AWS Lambda layer, which provides the awscli and kubectl
executables.

To use as a standalone AWS lambda function, use the `lambdaHandler` method.

To use as an AWS Lambda-backed CloudFormation custom resource handler, use the 
`cfnHandler` method as a `ServiceToken` backend.
"""
import os, json
import boto3
import _helper, _cfnresponse




class LambdaHandlerException( BaseException ):
    """Exception that occurs during a shell command operation
    """



def lambdaHandler( event, context ):
    """generic handler for AWS Lambda function
    """

    assert 'ResourceProperties' in event.keys()
    assert 'RequestType' in event.keys()

    if event[ 'RequestType' ] in [ 'Update', 'Delete' ]:

        assert 'PhysicalResourceId' in event.keys()

    client = boto3.client( 'ec2' )

    try:

        _helper.isVpnConnectionParamsDict( event[ 'ResourceProperties' ] )

        kwargs = _helper.convert( event[ 'ResourceProperties' ] )
    except BaseException as e:

        return {
            'statusCode': 500,
            'headers': { 'Content-Type': 'text/plain' },
            'body': f"ResourceProperties Error: %s" % ( str( e ) )
        }

    try:

        if event[ 'RequestType' ] in [ 'Create' ]:

            response = client.create_vpn_connection( **kwargs )
        elif event[ 'RequestType' ] in [ 'Update' ]:

            gtype, gid = _helper.findSingleNeedleInHaystack( 
                [ 'TransitGatewayId', 'VpnGatewayId' ],
                kwargs
            )

            response = client.modify_vpn_connection(
                gtype = gid,
                CustomerGatewayId = kwargs[ 'CustomerGatewayId' ],
                VpnConnectionId = event[ 'PhysicalResourceId' ]
            )
        elif event[ 'RequestType' ] in [ 'Delete' ]:

            client.delete_vpn_connection(
                VpnConnectionId = event[ 'PhysicalResourceId' ]
            )

            response = {
                'VpnConnection': { 
                    'VpnConnectionId': event[ 'PhysicalResourceId']
                }
            }

        vpnConnectionId = response[ 'VpnConnection' ][ 'VpnConnectionId' ]
    except BaseException as e:

        return {
            'statusCode': 500,
            'headers': { 'Content-Type': 'text/plain' },
            'body': f"%s Error: %s" % ( event[ 'RequestType' ], str( e ) )
        }

    try:

        #state = _helper.waitForTerminalVpnConnectionStatus( 
        #    vpnConnectionId,
        #    client
        #)

        state = _helper.getVpnConnectionStatus(
            vpnConnectionId
        )

        if state not in [ 'available', 'pending' ]:

            raise BaseException( 'Invalid state: %s' % ( state ) )
    except BaseException as e:

        return {
            'statusCode': 500,
            'headers': { 'Content-Type': 'text/plain' },
            'body': f"VPN Connection State Error: %s" % ( str( e ) )
        }

    return {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps( {
            'VpnConnectionId': vpnConnectionId
        } )
    }



def cfnHandler( event, context ):
    """handler for Lambda-backed CloudFormation custom resources
    """

    try:

        execresponse = lambdaHandler( event, context )

    except BaseException as e:

        responseData = {
            'Reason': '%s: %s' % ( e.__class__.__name__, str( e ) )
        }

        _cfnresponse.send( event, context, _cfnresponse.FAILED, responseData, '' )

    if execresponse[ 'statusCode' ] != 200:

        responseData = {
            'Reason': execresponse[ 'body' ]
        }

        _cfnresponse.send( event, context, _cfnresponse.FAILED, responseData, '' )

    else:

        vpnConnectionId = json.loads( execresponse[ 'body' ] )[ 'VpnConnectionId' ]

        responseData = {
            'Reason': 'VPN Connection successfully created.'
        }

        _cfnresponse.send( 
            event,
            context,
            _cfnresponse.SUCCESS,
            responseData,
            vpnConnectionId
        )