#!/usr/bin/env python3
import subprocess, os, tempfile, warnings
import boto3, botocore.exceptions



def findSingleNeedleInHaystack( needles, haystack ):
    """ find a single string within a dict's list of keys

    :param needles: a list of strings to be compared against a dict's keys
    :type needles: list
    :param haystack: a dictionary with keys to be compared against a list of
        string
    :type haystack: dict

    :raises KeyError: multiple needles in haystack

    :return: matched needle value and key
    :rtype: tuple
    """

    key = None

    for needle in needles:

        if needle in haystack.keys():

            if key != None: raise KeyError( '' )

            key = needle

    return key, haystack[ key ]



def _isTagList( inp ):

    if isinstance( inp, list ):

        for item in inp:

            if 'Key' in item.keys() and 'Value' in item.keys():

                continue

            else: return False

        return True

    return False



def _isValueDictList( inp, type ):
    """
    """

    if isinstance( inp, list ):

        for item in inp:

            if isinstance( item, dict ) and 'Value' in item.keys() \
               and isinstance( item[ 'Value' ], type ): continue

            else: return False

        return True

    return False



def _isStringValueDictList( inp ):
    """
    """

    return _isValueDictList( inp, str )



def _isIntegerValueDictList( inp ):
    """
    """

    return _isValueDictList( inp, int )



def _isBoolean( inp ):

    if isinstance( inp, bool ): return True

    elif not isinstance( inp, str ): return False

    if inp in [ 'True', 'False', 'false', 'true', 'TRUE', 'FALSE' ]:

        return True

    return False



def _toBoolean( inp ):

    if isinstance( inp, bool ): return inp

    if inp in [ 'True', 'true', 'TRUE' ]: return True

    elif inp in [ 'False', 'false', 'FALSE' ]: return False

    raise BaseException()



def _isNumber( inp ):

    try:
        float( inp )

        return True

    except ValueError:

        return False



def _isType( val, _type ):
    return {
        'Boolean': _isBoolean,
        'String': lambda x: isinstance( x, str ),
        'Number': _isNumber,
        'String[]': _isStringValueDictList,
        'Number[]': _isIntegerValueDictList,
        'Tags': _isTagList,
        'VpnTunnelOptionsSpecifications': _isVpnTunnelOptionsSpecificationsDict
    }[ _type ]( val )



def _convType( val, _type ):
    return {
        'Boolean': _toBoolean,
        'String': str,
        'Number': float,
        'String[]': lambda x: [ str(_x) for _x in x ],
        'Number[]': lambda x: [ float(_x) for _x in x ],
        'Tags': _interpolateTagSpecifications,
        'VpnTunnelOptionsSpecifications': _interpolateTunnelOptionsSpecifications
    }[ _type ]( val )



STRUCT_CONNECTION = {
    'EnableAcceleration': {
        'type': 'Boolean',
        'required': False
    },
    'StaticRoutesOnly': {
        'type': 'Boolean',
        'required': False
    },
    'TunnelInsideIpVersion': {
        'type': 'String',
        'required': False
    },
    'LocalIpv4NetworkCidr': {
        'type': 'String',
        'required': False
    },
    'RemoteIpv4NetworkCidr': {
        'type': 'String',
        'required': False
    },
    'LocalIpv6NetworkCidr': {
        'type': 'String',
        'required': False
    },
    'RemoteIpv6NetworkCidr': {
        'type': 'String',
        'required': False
    }
}



STRUCT_TUNNEL = {
        'TunnelInsideCidr': {
            'type': 'String',
            'required': False
        },
        'TunnelInsideIpv6Cidr': {
            'type': 'String',
            'required': False
        },
        'PreSharedKey': {
            'type': 'String',
            'required': False
        },
        'Phase1LifetimeSeconds': {
            'type': 'Number',
            'required': False
        },
        'Phase2LifetimeSeconds': {
            'type': 'Number',
            'required': False
        },
        'RekeyMarginTimeSeconds': {
            'type': 'Number',
            'required': False
        },
        'RekeyFuzzPercentage': {
            'type': 'Number',
            'required': False
        },
        'ReplayWindowSize': {
            'type': 'Number',
            'required': False
        },
        'DPDTimeoutSeconds': {
            'type': 'Number',
            'required': False
        },
        'DPDTimeoutAction': {
            'type': 'String',
            'required': False
        },
        'Phase1EncryptionAlgorithms': {
            'type': 'String[]',
            'required': False
        },
        'Phase2EncryptionAlgorithms': {
            'type': 'String[]',
            'required': False
        },
        'Phase1IntegrityAlgorithms': {
            'type': 'String[]',
            'required': False
        },
        'Phase2IntegrityAlgorithms': {
            'type': 'String[]',
            'required': False
        },
        'Phase1DHGroupNumbers': {
            'type': 'Number[]',
            'required': False
        },
        'Phase2DHGroupNumbers': {
            'type': 'Number[]',
            'required': False
        },
        'IKEVersions': {
            'type': 'String[]',
            'required': False
        },
        'StartupAction':  {
            'type': 'String',
            'required': False
        }
    }

def _isVpnTunnelOptionsSpecificationsDict( inp ):

    struct = STRUCT_TUNNEL

    if isinstance( inp, list ):

        for item in inp:

            try:

                _typecastStructDict( struct, item )

            except BaseException as e:

                raise ValueError( item )

        return True

    return False



STRUCT_RESOURCE = {
    'CustomerGatewayId': {
        'type': 'String'
    },
    'Tags': {
        'type': 'Tags',
        'required': False
    },
    'TransitGatewayId': {
        'type': 'String',
        'required': False
    },
    'Type': {
        'type': 'String'
    },
    'VpnGatewayId': {
        'type': 'String',
        'required': False
    },
    'VpnTunnelOptionsSpecifications': {
        'type': 'VpnTunnelOptionsSpecifications',
        'required': False
    },
    **STRUCT_CONNECTION
}



def isVpnConnectionParamsDict( inp ):

    struct = STRUCT_RESOURCE

    inp = _typecastStructDict( struct, inp )

    findSingleNeedleInHaystack( [ 'TransitGatewayId', 'VpnGatewayId' ], inp )



def _typecastStructDict( struct, inp ):

    assert isinstance( struct, dict )

    for structName, structDef in struct.items():

        if ( 'required' not in structDef.keys() or 
            structDef[ 'required' ] == True ) and structName not in inp.keys():

            raise KeyError( structName )

        if structName in inp.keys():

            if _isType( inp[ structName ], structDef[ 'type' ] ) != True:
            #if structDef[ 'type' ]( inp[ structName ] ) != True:

                raise ValueError( structName )

            inp[ structName ] = _convType( inp[ structName ], structDef[ 'type' ] )


    return inp



def _interpolateTunnelOptionsSpecifications( inp ):

    options = []

    #for each specification
    for i in range( len( inp ) ):

        spec = inp[ i ]

        for key in STRUCT_TUNNEL.keys():

            if key in spec.keys():

                if len( options ) - 1 <= i:

                    options.append( { } )

                options[ i ][ key ] = inp[ i ][ key ]

    return options



def _interpolateTagSpecifications( inp ):

    return [
        {
            'Tags': inp
        }
    ]



def convert( inp ):

    struct = STRUCT_RESOURCE

    inp = _typecastStructDict( struct, inp )

    gType, gId = findSingleNeedleInHaystack(
        [ 'TransitGatewayId', 'VpnGatewayId' ],
        inp
    )

    kwargs = {
        gType: gId,
        'Type': inp[ 'Type' ],
        'CustomerGatewayId': inp[ 'CustomerGatewayId' ],
        'Options': {}
    }

    for key in STRUCT_CONNECTION.keys():

        if key in inp.keys():

            kwargs[ 'Options' ][ key ] = inp[ key ]

    if 'Tags' in inp.keys():

        kwargs[ 'TagSpecifications' ] = inp[ 'Tags' ]

    if 'VpnTunnelOptionsSpecifications' in inp.keys() \
        and len( inp[ 'VpnTunnelOptionsSpecifications'] ) > 0:

        kwargs[ 'Options' ][ 'TunnelOptions' ] = inp[ 'VpnTunnelOptionsSpecifications' ]

    return kwargs



def getVpnConnectionStatus( connectionId, client = None ):

    client = client if client != None else boto3.client( 'ec2' )

    response = client.describe_vpn_connections(
        VpnConnectionIds = [ connectionId ]
    )

    return response[ 'VpnConnections' ][ 0 ][ 'State' ]



def waitForTerminalVpnConnectionStatus( connectionId, client = None, wait = 10 ):

    import time

    client = client if client != None else boto3.client( 'ec2' )

    nonTerminalStatus = [ 'deleting', 'pending' ]

    while getVpnConnectionStatus( connectionId, client ) in nonTerminalStatus:

        time.sleep( wait )

    return getVpnConnectionStatus( connectionId, client )