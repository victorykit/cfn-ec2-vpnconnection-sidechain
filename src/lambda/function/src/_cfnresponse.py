#!/usr/bin/env python3
import urllib.request, urllib.error, pprint, json


SUCCESS = "SUCCESS"
FAILED = "FAILED"

def send( event, context, status, data, prid = None, noEcho = False ):

    rurl=event['ResponseURL']

    print(f"Response URL: {rurl}")

    rb = {}

    rb['Status'] = status

    rb['Reason'] = data[ 'Reason' ] or 'See the details in CloudWatch Log Stream: ' + context.log_stream_name

    rb['PhysicalResourceId'] = prid or context.log_stream_name

    rb['StackId'] = event['StackId']

    rb['RequestId'] = event['RequestId']

    rb['LogicalResourceId'] = event['LogicalResourceId']

    rb['NoEcho'] = noEcho

    rb['Data'] = data

    pprint.pprint( rb )

    rbd = json.dumps( rb )

    hdrs = { 'content-type' : '', 'content-length' : str( len( rbd ) ) }

    try:

        request = urllib.request.Request(method = 'PUT',url = rurl,headers = hdrs,data = rbd.encode( 'utf-8' ),)

        s = urllib.request.urlopen( request ); s.close()

    except urllib.error.HTTPError as e: print( f'HTTPError: {e.code}' )

    except urllib.error.URLError as e: print( f'URLError: {e.reason}' )
