#!/usr/bin/env python3
"""
"""
import pytest, os


@pytest.fixture
def module():

    import _helper as module

    return module



@pytest.fixture
def mock():

    import unittest.mock

    return unittest.mock



class Test_findSingleNeedleInHaystack():


    def test_default( self, module ):

        result = module.findSingleNeedleInHaystack( 
            [ 'TransitGatewayId', 'VpnGatewayId' ],
            {
                'TransitGatewayId': 'foobar'
            }
        )

        assert result == ( 'TransitGatewayId', 'foobar' )


    def test_failDueToMultipleGatewaysSpecified( self, module ):

        with pytest.raises( BaseException ):

            module.findSingleNeedleInHaystack( 
                [ 'TransitGatewayId', 'VpnGatewayId' ],
                {
                    'TransitGatewayId': 'foobar',
                    'VpnGatewayId': 'barfoo'
                }
            )



class Test__isTagList():

    def test_default( self, module ):

        tagList = [
            {
                'Key': 'Foo',
                'Value': 'Bar'
            }
        ]

        result = module._isTagList( tagList )

        assert result == True


    def test_missingKeyInTagSpec( self, module ):

        tagList = [
            {
                'Key': 'Foo',
            }
        ]

        result = module._isTagList( tagList )

        assert result == False



class Test__isValueDictList():

    def test_default( self, module ):

        result = module._isValueDictList( [
            {
                'Value': 'foo'
            }
        ], str )

        assert result == True


    def test_faultyType( self, module ):

        result = module._isValueDictList( [
            {
                'Value': 'foo'
            }
        ], int )

        assert result == False


    def test_missingValueKey( self, module ):

        result = module._isValueDictList( [
            {
                'Foo': 'foo'
            }
        ], int )

        assert result == False



class Test__isBoolean():

    def test_default( self, module ):

        assert module._isBoolean( True )
        assert module._isBoolean( False )


    def test_string( self, module ):

        assert module._isBoolean( 'true' ) == True
        assert module._isBoolean( 'false' ) == True
        assert module._isBoolean( 'True' ) == True
        assert module._isBoolean( 'False' ) == True
        assert module._isBoolean( 'TRUE' ) == True
        assert module._isBoolean( 'FALSE' ) == True


    def test_fault( self, module ):

        assert module._isBoolean( 'tRue' ) == False
        assert module._isBoolean( 1 ) == False



class Test__isNumber():

    def test_default( self, module ):

        assert module._isNumber( 0 )
        assert module._isNumber( 1 )
        assert module._isNumber( 1.01 )
        assert module._isNumber( -1.01 )
        assert module._isNumber( -1 )


    def test_string( self, module ):

        assert module._isNumber( '0' )
        assert module._isNumber( '1' )
        assert module._isNumber( '1.01' )
        assert module._isNumber( '-1.01' )
        assert module._isNumber( '-1' )


    def test_fault( self, module ):

        assert module._isNumber( 'foo' ) == False
        assert module._isNumber( '-' ) == False
        assert module._isNumber( 'nil' ) == False



class Test__isStringValueDictList():

    def test_default( self, module ):

        result = module._isStringValueDictList( [
            {
                'Value': 'foo'
            }
        ] )

        assert result == True


    def test_faultyType( self, module ):

        result = module._isStringValueDictList( [
            {
                'Value': 123
            }
        ] )

        assert result == False



class Test__isIntegerValueDictList():

    def test_default( self, module ):

        result = module._isIntegerValueDictList( [
            {
                'Value': 123
            }
        ] )

        assert result == True


    def test_faultyType( self, module ):

        result = module._isIntegerValueDictList( [
            {
                'Value': 'foo'
            }
        ] )

        assert result == False



class Test_isVpnConnectionParamsDict():

    def test_default( self, module ):

        resourceProperties = {
            'CustomerGatewayId': 'foo',
            'StaticRoutesOnly': True,
            'Tags': [],
            'Type': 'foofoobar',
            'VpnGatewayId': 'foofoobarbar',
            'VpnTunnelOptionsSpecifications': [],
        }

        result = module.isVpnConnectionParamsDict( resourceProperties )

        assert result == None


    def test_failDueToMissingRequired( self, module ):


        with pytest.raises( KeyError ):

            module.isVpnConnectionParamsDict( {} )


    def test_failDueToFaultyValue( self, module ):


        with pytest.raises( ValueError ):

            module.isVpnConnectionParamsDict( {
            'CustomerGatewayId': {},
            'StaticRoutesOnly': True,
            'Tags': [],
            'TransitGatewayId': 'foobart',
            'Type': 'foofoobar',
            'VpnGatewayId': 'foofoobarbar',
            'VpnTunnelOptionsSpecifications': []
        } )




class Test__validateStruct():

    def test_default( self, module ):

        struct = {
            'Foo': {
                'type': 'String',
                'required': False
            }
        }

        inp = {}

        assert module._typecastStructDict( struct, inp ) == {}


    def test_default1( self, module ):

        struct = {
            'Foo': {
                'type': 'String',
                'required': True
            }
        }

        inp = {
            'Foo': 'foo'
        }

        assert module._typecastStructDict( struct, inp ) == {
            'Foo': 'foo'
        }


    def test_default2( self, module ):

        struct = {
            'Foo': {
                'type': 'String',
            }
        }

        inp = {
            'Foo': 'foo'
        }

        assert module._typecastStructDict( struct, inp ) == {
            'Foo': 'foo'
        }


    def test_typecast( self, module ):

        struct = {
            'Foo': {
                'type': 'Boolean',
            }
        }

        inp = {
            'Foo': 'True'
        }

        assert module._typecastStructDict( struct, inp ) == {
            'Foo': True
        }
    
    
    def test_typecastTunnelOptions( self, module ):

        struct = {
            'Foo': {
                'type': 'VpnTunnelOptionsSpecifications',
            }
        }

        inp = {
            'Foo': [
                {
                    'TunnelInsideCidr': 'foofoo'
                },
                {
                    'TunnelInsideCidr': 'foofoo'
                }
            ]
        }

        assert module._typecastStructDict( struct, inp ) == inp


    def test_failDueToNoTypeSpecification( self, module ):

        struct = {
            'Foo': {}
        }

        inp = {
            'Foo': 'foo'
        }

        with pytest.raises( KeyError ):

            module._typecastStructDict( struct, inp )


    def test_failDueToTypeError( self, module ):

        struct = {
            'Foo': {
                'type': 'String',
            }
        }

        inp = {
            'Foo': 123
        }

        with pytest.raises( ValueError ):

            module._typecastStructDict( struct, inp )



class Test__interpolateTunnelOptionsSpecifications():

    def test_default( self, module ):

        inp = [
            {
                'TunnelInsideCidr': 'foofoo'
            }
        ]

        out = [
            {
                'TunnelInsideCidr': 'foofoo'
            }
        ]

        assert inp == out



class Test_convert():

    def test_default( self, module ):

        inp = {
            'CustomerGatewayId': 'foo',
            'StaticRoutesOnly': 'True',
            'Tags': [
                {
                    'Key': 'foo',
                    'Value': 'bar'
                }
            ],
            'Type': 'foofoobar',
            'VpnGatewayId': 'foofoobarbar',
            'VpnTunnelOptionsSpecifications': [
                {
                    'TunnelInsideCidr': 'foofoo'
                }
            ]
        }

        out = {
            'CustomerGatewayId': inp[ 'CustomerGatewayId' ],
            'VpnGatewayId': inp['VpnGatewayId'],
            'Type': inp[ 'Type' ],
            'TagSpecifications': [
                {
                    'Tags': [
                        {
                            'Key': 'foo',
                            'Value': 'bar'
                        }
                    ]
                }
            ],
            'Options': {
                'StaticRoutesOnly': True,
                'TunnelOptions': [
                    {
                        'TunnelInsideCidr': 'foofoo'
                    }
                ]
            }
        }

        assert module.convert( inp ) == out



class Test_getVpnConnectionStatus():


    def _mockTerminalResponse( self, operationName, kwargs ):

        if operationName == 'DescribeVpnConnections':
            return {
                'VpnConnections': [
                    {
                        'VpnConnectionId': kwargs[ 'VpnConnectionIds' ][ 0 ],
                        'State': 'pending'
                    }
                ]
            }
        else:
            raise BaseException( 'Wrong command: %s' % ( operationName ) )


    def test_existingVpnConnectionWithDefaultClient( self, module, mock ):

        import os
        os.environ[ 'AWS_DEFAULT_REGION' ] = 'eu-central-1'

        with mock.patch(
            'botocore.client.BaseClient._make_api_call',
            new=self._mockTerminalResponse
        ):

            result = module.getVpnConnectionStatus( 'foo' )

            assert result == 'pending'


    def test_existingVpnConnectionWithCustomClient( self, module, mock ):

        import os
        os.environ[ 'AWS_DEFAULT_REGION' ] = 'eu-central-1'

        with mock.patch(
            'botocore.client.BaseClient._make_api_call',
            new=self._mockTerminalResponse
        ):

            import boto3

            client = boto3.client( 'ec2' )

            result = module.getVpnConnectionStatus( 'foo', client )

            assert result == 'pending'



class Test_waitForTerminalVpnConnectionStatus():

    _nonTerminalRoundTripIndex = 0

    def _mockNonTerminalResponse( self, operationName, kwargs ):

        if operationName == 'DescribeVpnConnections':

            if self._nonTerminalRoundTripIndex < 2 and \
               self._nonTerminalRoundTripIndex >= 0:

                self._nonTerminalRoundTripIndex += 1

                return {
                    'VpnConnections': [
                        {
                            'VpnConnectionId': kwargs[ 'VpnConnectionIds' ][ 0 ],
                            'State': 'pending'
                        }
                    ]
                }
            elif self._nonTerminalRoundTripIndex == 2:

                self._nonTerminalRoundTripIndex = -1

                return {
                    'VpnConnections': [
                        {
                            'VpnConnectionId': kwargs[ 'VpnConnectionIds' ][ 0 ],
                            'State': 'available'
                        }
                    ]
                }
            else:

                self._nonTerminalRoundTripIndex = 0

                return {
                    'VpnConnections': [
                        {
                            'VpnConnectionId': kwargs[ 'VpnConnectionIds' ][ 0 ],
                            'State': 'available'
                        }
                    ]
                }
        else:
            raise BaseException( 'Wrong command: %s' % ( operationName ) )


    def test_existingVpnConnectionWithDefaultClient( self, module, mock ):

        import os
        os.environ[ 'AWS_DEFAULT_REGION' ] = 'eu-central-1'

        with mock.patch(
            'botocore.client.BaseClient._make_api_call',
            new=self._mockNonTerminalResponse
        ):

            result = module.waitForTerminalVpnConnectionStatus( 'foo' )

            assert result == 'available'


    def test_existingVpnConnectionWithCustomClient( self, module, mock ):

        import os
        os.environ[ 'AWS_DEFAULT_REGION' ] = 'eu-central-1'

        with mock.patch(
            'botocore.client.BaseClient._make_api_call',
            new=self._mockNonTerminalResponse
        ):

            import boto3

            client = boto3.client( 'ec2' )

            result = module.waitForTerminalVpnConnectionStatus( 'foo' )

            assert result == 'available'


    def test_existingVpnConnectionWithCustomWaitPeriod( self, module, mock ):

        import os
        os.environ[ 'AWS_DEFAULT_REGION' ] = 'eu-central-1'

        with mock.patch(
            'botocore.client.BaseClient._make_api_call',
            new=self._mockNonTerminalResponse
        ):

            result = module.waitForTerminalVpnConnectionStatus( 'foo', None, 1 )

            assert result == 'available'